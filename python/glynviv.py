# GlynVivPi
#(c) Totga Games for Glyn Vivian Gallery
#tim@totga-games.com

#
# This is a simple falling items game made for the Raspberry Pi programming workshop for
# the Richard Glynn Vivian gallery in Swansea, South Wales.
#

# Richard Glynn Vivian was a patron of the arts who founded the gallery at the start of the 20th century. 
# Whilst he unfortunately never lived to see its completion, the gallery has been enjoyed by generations since.
# In our game, we've taken a looser interpretation of history and have cast our philatrophical art patron as a 
# litteral defender of the arts: catching priceless artefacts being thrown from the roof of the building by 
# a darstedly (although slightly blind and hapless) art thief who believes they are being caught by his partner in crime.



import pygame, sys, random
from pygame.locals import *
import SpriteSheet
import SpriteStripAnim
import TSprite
import math
#from Sprite import Sprite
import veclib
import types
import random

#constants
WINDOWWIDTH = 1024	
WINDOWHEIGHT = 768

HALFW = WINDOWWIDTH / 2.0
HALFH = WINDOWHEIGHT / 2.0

FONT_SIZE = 20

FPS = 30

MC_LEFT = 1
MC_RIGHT = 2
MC_UP = 4
MC_DOWN = 8
GROUND_LEVEL = 630      #ground level is at 630 units - we should rework the sprite code to allow for different hotspots and place the ground at zero


WHITE = (255,255,255)

TEXTCOLOR = WHITE

WORLD_LEFT = -1000
WORLD_RIGHT = 1000
JUMP_THRESHOLD = 2
GALLERY_ROOF_HEIGHT = 280
GALLERY_ROOF_WIDTH = 700
GALLERY_ROOF_START_X = 200
DROP_WAIT_TIME = 2500.0
DROP_WAIT_TIME_VAR = 0.5
JUMP_STRENGTH = 30.0

GRAVITY = 0.5
MOVESPEED = 1

CURSOR_MOVE_KEYS = [K_LEFT, K_RIGHT, K_UP, K_DOWN]
WASD_MOVE_KEYS = [K_a, K_d, K_w, K_s]
DESTROY_WAIT_TIME = 200

#global variables
player_object = None                                #player sprite


backgroundsprites = pygame.sprite.OrderedUpdates()
artefactlist = pygame.sprite.Group()                #The priceless artefacts that are dropped from the roof
foregroundsprites = pygame.sprite.Group()
destroysprites = pygame.sprite.Group()
spritelist = pygame.sprite.OrderedUpdates()


def movecode_to_horizontal_movement(mvcode):
    if (mvcode & MC_LEFT) != 0:
        return -MOVESPEED
    elif (mvcode & MC_RIGHT) != 0:
        return MOVESPEED
    else:
        return 0

def movecode_to_vertical_movement(mvcode):
    if (mvcode & MC_UP) != 0:
        return -MOVESPEED
    elif (mvcode & MC_DOWN) != 0:
        return MOVESPEED
    else:
        return 0


class Artefact(object):
    def __init__(self, image, collectimage, destroyimage, value, weight):
        self.image = image
        self.value = value
        self.collectimage = collectimage
        self.destroyimage = destroyimage
        self.weight = weight


class DisappearSprite(TSprite.PhysSprite):   
    def __init__(self, image, pos, depth):
        TSprite.PhysSprite.__init__(self, image, pos, depth)
        self.time = time = pygame.time.get_ticks()
        
    def update(self, camera):
        self.thrust = (self.thrust[0], self.thrust[1] + GRAVITY) #apply gravity
        TSprite.PhysSprite.update(self, camera)    
        if (pygame.time.get_ticks() - self.time > DESTROY_WAIT_TIME):
            self.kill()

class PlayerSpriteNoAnim(TSprite.PhysSprite):   
    def __init__(self, image, pos, depth):
        TSprite.PhysSprite.__init__(self, image, pos, depth)
        self.score = 0
        self.health = 100

    def update(self, camera):
        self.thrust = (self.thrust[0], self.thrust[1] + GRAVITY) #apply gravity
        TSprite.PhysSprite.update(self, camera)    
        if self.bounds.bottom > GROUND_LEVEL:
            pos = self.get_pos()
            self.set_pos((pos[0], GROUND_LEVEL - self.bounds.height))     

class RobberSprite(TSprite.AnimSprite):   
    def __init__(self, images, pos, depth):
        TSprite.AnimSprite.__init__(self, images, pos, depth)

    def update(self, camera):
        self.thrust = (self.thrust[0], self.thrust[1] + GRAVITY) #apply gravity
        TSprite.PhysSprite.update(self, camera)    
          
class PlayerSprite(TSprite.AnimSprite):   
    def __init__(self, images, pos, depth):
        TSprite.AnimSprite.__init__(self, images, pos, depth)
        self.score = 0
        self.health = 100
        self.movecode = 0


    def jump():
        if math.fabs(self.bounds.bottom - GROUND_LEVEL) < JUMP_THRESHOLD:
             self.thrust = (self.thrust[0], self.thrust[1] - JUMP_STRENGTH) #apply gravity


    def update(self, camera):
        self.thrust = (self.thrust[0], self.thrust[1] + GRAVITY) #apply gravity
        if self.movecode != 0:     #globals       
            if (self.movecode & MC_RIGHT) != 0:
                self.set_anim_params(1, 10, 0.1)
            else:
                self.set_anim_params(11, 10, 0.1)
            thrustamount = (movecode_to_horizontal_movement(self.movecode), movecode_to_vertical_movement(self.movecode))
            self.apply_thrust( thrustamount)
        else:
            self.set_anim_params(0, 1, 0.1)

        TSprite.AnimSprite.update(self, camera)    
        if self.bounds.bottom > GROUND_LEVEL:
            pos = self.get_pos()
            self.set_pos((pos[0], GROUND_LEVEL - self.bounds.height))     

class ArtefactSprite(TSprite.PhysSprite):    
    def __init__(self, image, pos, depth, type):
        TSprite.PhysSprite.__init__(self, image, pos, depth)
        self.type = type

    def update(self, camera):
        self.thrust = (self.thrust[0], self.thrust[1] + GRAVITY) #apply gravity
        TSprite.PhysSprite.update(self, camera)    
        if (self.rect.colliderect(player_object.rect)):
            player_object.score += self.type.value            
            self.kill()             
        elif self.bounds.bottom > GROUND_LEVEL:
            player_object.score -= self.type.value
            destroysprites.add(DisappearSprite(self.type.destroyimage, self.pos, self.depth))
            self.kill()        
        

artefact_type_list = []     #list of artefact types. Adding to this list makes new artefacts for our thief to drop



#This overrides the update method of the 'cloud' and causes it to animate across the screen
def wrapmove_update(self, camera):
    TSprite.DepthSprite.update(self, camera)    
    pos = self.get_pos()
    newpos = (pos[0] - 10.0, pos[1])
    if newpos[0]  < WORLD_LEFT  / self.depth- self.rect.width:
        newpos = (WORLD_RIGHT / self.depth, pos[1])
    self.set_pos(newpos)

    

class Camera(object):    
    def __init__(self, pos):        
        self.offset = (HALFW, HALFH)
        self.pos = pos
        self.target = None
        self.speed = 0.05
    def update(self):
        diff = (self.pos[0] - self.target.pos[0], self.pos[1] - self.target.pos[1])
        length = veclib.veclength(diff)
        if length != 0:
            norm = diff#veclib.normalized2(diff)updatebounds()
            addv = (norm[0] * self.speed, norm[1] * self.speed)
            self.pos = (self.pos[0] - addv[0], self.pos[1] - addv[1])



def reset_sprites(spritelist):
    for s in spritelist:
        s.reset()

def set_move_code(key, movekeys, movecode):    
    if key  == movekeys[0]:
        movecode |= MC_LEFT    
    elif key == movekeys[1]:
        movecode |= MC_RIGHT
    elif key == movekeys[2]:
        movecode |= MC_UP
    elif key == movekeys[3]:
        movecode |= MC_DOWN
    return movecode

def clear_move_code(key, movekeys, movecode):    
    if key  == movekeys[0]:
        movecode &= ~MC_LEFT    
    elif key == movekeys[1]:
        movecode &= ~MC_RIGHT
    elif key == movekeys[2]:
        movecode &= ~MC_UP
    elif key == movekeys[3]:
        movecode &= ~MC_DOWN
    return movecode

last_time_drop = 0

def process_artefact_drop():
    global last_time_drop
    global DROP_WAIT_TIME
    time = pygame.time.get_ticks()
    wait_time = DROP_WAIT_TIME + DROP_WAIT_TIME * DROP_WAIT_TIME_VAR * random.random()
    if (time - last_time_drop) > wait_time:
        #print(time)
        artefact_idx = random.randint(0, len(artefact_type_list) - 1)
        artefact_type = artefact_type_list[artefact_idx]
        randx = GALLERY_ROOF_START_X+(random.random()) * GALLERY_ROOF_WIDTH
        sprite = ArtefactSprite(artefact_type.image, (randx, GALLERY_ROOF_HEIGHT), 1.0, artefact_type)        
        sprite.thrust = ((random.random() - 0.5) * 20.0, 0)
        sprite.damp = 0.2
        artefactlist.add(sprite)
        last_time_drop = time
        DROP_WAIT_TIME -= 10

        if (DROP_WAIT_TIME < 100):
            DROP_WAIT_TIME = 100

EXIT = False

def main():
    global FPSCLOCK, DISPLAYSURF, font, RESET_SURF, RESET_RECT, NEW_SURF, NEW_RECT, SOLVE_SURF, SOLVE_RECT    
    global movecode
    global player_object
    global spritelist


    
    camera = Camera((0,0))
    camera.offset = (camera.offset[0], camera.offset[1] + 100)

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT)) #pygame.FULLSCREEN
    pygame.display.set_caption('Glyn Vivian rescues Priceless Art!')
    font = pygame.font.Font('freesansbold.ttf', FONT_SIZE)


    #animsprites = anim.load_strip((0,0,32,64), 22)    

    robbersheet = SpriteSheet.SpriteSheet('img/robber.png')
    robbersprites = robbersheet.load_strip((0,0,32,64), 3)
    
    walkcyclesheet = SpriteSheet.SpriteSheet('img/walkcycle.png')
    walkcyclesprites = walkcyclesheet.load_strip((0,0,32,64), 22)

    artefact_type_list.append(Artefact(pygame.image.load('img/painting1.png'), None, pygame.image.load('img/painting1_cracked.png'), 100, 1))
    #artefact_type_list.append(Artefact(pygame.image.load('img/painting2.png'), None, pygame.image.load('img/painting1_cracked.png'), 1000, 1))
    #artefact_type_list.append(Artefact(pygame.image.load('img/painting3.png'), None, pygame.image.load('img/painting1_cracked.png'), 500, 1))
    #artefact_type_list.append(Artefact(pygame.image.load('img/painting4.png'), None, pygame.image.load('img/painting1_cracked.png'), 200, 1))
    #artefact_type_list.append(Artefact(pygame.image.load('img/painting5.png'), None, pygame.image.load('img/painting1_cracked.png'), 120, 1))
    #artefact_type_list.append(Artefact(pygame.image.load('img/teapot.png'), 10, 0.3))
    artefact_type_list.append(Artefact(pygame.image.load('img/vase.png'), None, pygame.image.load('img/vase_broken.png'), 1000, 10))
    
    cloud = TSprite.PhysSprite(pygame.image.load('img/cloud1.png').convert_alpha(), (0, -3500), 0.1)
    cloud.update = types.MethodType(wrapmove_update, cloud)
    gallery = TSprite.DepthSprite(pygame.image.load('img/galleryfront.png').convert_alpha(), (-500, 0), 1.0)
    gallery.set_pos((0, gallery.bounds.height * 0.5 - 10))

    groundimg = pygame.image.load('img/ground.png').convert_alpha()
    treeimg   = pygame.image.load('img/tree.png').convert_alpha()
    tree_ground = treeimg.get_height() + 20
    tree =  TSprite.DepthSprite(treeimg, (-300, tree_ground), 1.5)    
    tree2 =  TSprite.DepthSprite(treeimg, (260, tree_ground), 1.5)        
    tree3 =  TSprite.DepthSprite(treeimg, (800, tree_ground), 1.5)        

    backgroundsprites.add(cloud)
    backgroundsprites.add(gallery)
    
    skygrad = pygame.image.load('img/skygrad.png').convert_alpha()
    skygrad = pygame.transform.scale(skygrad, (WINDOWWIDTH, WINDOWHEIGHT))
    
    player_object = PlayerSprite(walkcyclesprites,(0,550),1.0)    
    player_object.anim_speed = 0.1
    player_object.damp = 0.1
    spritelist.add(player_object)
    
    #testing
    walkcycletest = TSprite.AnimSprite(walkcyclesprites, (0,240), 1.0)
    walkcycletest.set_anim_params(1, 8, 0.1)
    spritelist.add(walkcycletest)
    spritelist.add(TSprite.AnimSprite(robbersprites, (40,240), 1.0))
    xbase = -400
    
    foregroundsprites.add(tree)
    foregroundsprites.add(tree2)
    foregroundsprites.add(tree3)
    
    camera.target = player_object
    
    images = []
        
    
    treemovecode = 0;
    while EXIT == False:
      
        reset_sprites(spritelist.sprites())
        reset_sprites(artefactlist.sprites())        
        reset_sprites(destroysprites.sprites())
        DISPLAYSURF.blit(skygrad, (0,0), (0,0,WINDOWWIDTH, WINDOWHEIGHT))

        for event in pygame.event.get(): # event handling loop
            if event.type == KEYDOWN:
                player_object.movecode = set_move_code(event.key, CURSOR_MOVE_KEYS, player_object.movecode)               
                if event.key == K_SPACE:                   
                    player_object.jump()                    
                if event.key == K_ESCAPE:
                    terminate()
            elif event.type == KEYUP:
                player_object.movecode = clear_move_code(event.key, CURSOR_MOVE_KEYS, player_object.movecode)                
            elif event.type == QUIT:
                terminate()
                
        pygame.event.post(event) # put the other KEYUP event objects back
                        
        process_artefact_drop()
        
        checkForQuit()
        camera.update()
        backgroundsprites.update(camera)
        spritelist.update(camera)
        artefactlist.update(camera)
        destroysprites.update(camera) 
        foregroundsprites.update(camera)

        backgroundsprites.draw(DISPLAYSURF)
        spritelist.draw(DISPLAYSURF)
        artefactlist.draw(DISPLAYSURF)
        destroysprites.draw(DISPLAYSURF)
        foregroundsprites.draw(DISPLAYSURF)
        #DISPLAYSURF.blit(testtext_surf, (0,0))

        scoreSurf = font.render("Score: " +str(player_object.score), 1, TEXTCOLOR)
        scoreRect = scoreSurf.get_rect()
                #stepRect.bottomleft = (20, WINHEIGHT - 10)
        DISPLAYSURF.blit(scoreSurf, scoreRect)
     
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def terminate():
    pygame.quit()
    sys.exit()
    EXIT = True


def checkForQuit():
    for event in pygame.event.get(QUIT): # get all the QUIT events
        terminate() # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP): # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate() # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event) # put the other KEYUP event objects back




def makeText(text, color, bgcolor, top, left):
    # create the Surface and Rect objects for some text.
    textSurf = font.render(text, True, color, bgcolor)
    textRect = textSurf.get_rect()
    textRect.topleft = (top, left)
    return (textSurf, textRect)


if __name__ == '__main__':
    main()
