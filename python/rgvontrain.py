import random
import sys
print("Welcome to Glynn Vivian's Choose your own adventure!");

from util_adventure import *


 
MailCarriage = AdventureRoom("a mail carriage. There are many parcels locked behind a mesh door.")
FirstClassCarriage = AdventureRoom("a railway carriage. The light fittings are elegant, this must be the first class carriage")
DinningCarriage = AdventureRoom("the dinning carriage. The rich smell of food makes your mouth water")
SecondClassCarriage = AdventureRoom("the second class carriage. You notice that the fittings aren't quite as nice")

traindoor = "a standard issue train door"
AdventureRoomExit(DIR_EAST, MailCarriage, FirstClassCarriage, traindoor)
AdventureRoomExit(DIR_EAST, FirstClassCarriage, DinningCarriage, traindoor)
AdventureRoomExit(DIR_EAST, DinningCarriage, SecondClassCarriage, traindoor)

telephone = AdventureObject("Telephone", "A telephone", "This is a wealthy household: one of the first to have a telephone. Not too many people to phone at the moment.", 9)
telephonenotebook = AdventureObject("Notebook", "A Notebook", "Scribbled in black ink, the page is covered in dark marks", 9)
telephonetable = AdventureObject("Table", "A table", "A finely made Louis XV table: A stunning item, the owner has equisite taste.", 50)
telephonetable.add_object_on_top(telephone)
telephonetable.add_object_on_top(telephonenotebook)

buffettable = AdventureObject("Buffet Table", "A Buffet table", "A long table full of delicious food.", 50)

food_pork = AdventureObject("pork","a piece of pork","a succulent piece of pork that is sittig in the middle of various vegetables around it.")
food_turkey = AdventureObject("turkey","a pale wing","a succulent piece of turkey that is sittig in the middle of various vegetables around it.")
 
buffettable.add_object_on_top(food_pork)


envelope = AdventureObject("Envelope", "An envelope", "A brown envelope. There appears to be a letter inside", 1)

lettertext = "The letter reads thusly: \n Dear Victor Arbhuthnot esq. \n We are most disheartened to learn that you sullied the name of arbhuthnot with your scandelous action. \n Your actions are unbeffiting to a man of your sature. Return the stolen jewels immediately or there will be unfortunate consequences. \n Yours sincerely. Lord Englebert Rice \n"
letter = AdventureObject("Letter", "A Letter", lettertext, 1)

envelope.add_object_on_top(letter)
DinningCarriage.add_object(buffettable)

cupoftea = AdventureObject("Tea", "A cup of tea", "This seems to have been here for some time. I don't suppose it's very warm now", 1)
traintable1 = AdventureObject("table", "A utilitarian table", "A scuffed and well used train guard's table", 50)
traintable1.add_object_on_top(cupoftea)

traintable1.add_object_on_top(envelope)
MailCarriage.add_object(traintable1)

bed = AdventureObject("Bed"
, "An iron framed bed", "Fine victorian bed", 100)
bear = AdventureObject("Bear", "A teddy bear", "A well loved teddy bear. Missing one eye.", 1)

bed.add_object_on_top(bear)

set_current_room(MailCarriage)
print('You wake up in ')
get_current_room().printdescription()
while exit == False:
    
    input = raw_input("What do you want to do?")
    if get_current_room().respond(input) == False:
        introductions = ["You're in ", "You arrive in", "You are standing in", "You step in to ", "You are in ", "You have arrived in ", "You find yourself in "]
        print(introductions[random.randint(0, len(introductions) - 1)])
        get_current_room().printdescription()
