#!/usr/bin/python

# This is statement is required by the build system to query build info
if __name__ == '__build__':
	raise Exception


import sys
import veclib as vl

try:
  from OpenGL.GLUT import *
  from OpenGL.GL import *
  from OpenGL.GLU import *
except:
  print '''
ERROR: PyOpenGL not installed properly.  
        '''
  sys.exit()

rotate = [0, 0, 0]
spin = [0, 0, 0]

tracker_x = vl.normalized([0.586705446, 0.795832276, 0.149758935])
tracker_y = vl.normalized([-0.343306750, 0.0769454464, 0.936066091])
tracker_z = vl.normalized([0.126320183, 0.991366148, -0.0351626314])

def drawOneLine(x1, y1, x2, y2):
  glBegin(GL_LINES)
  glVertex2f(x1, y1)
  glVertex2f(x2, y2)
  glEnd()

def drawLine(x1, y1, z1, x2, y2, z2):
  glBegin(GL_LINES)
  glVertex3f(x1, y1, z1)
  glVertex3f(x2, y2, z2)
  glEnd()


def display2():
  glClear(GL_COLOR_BUFFER_BIT)

  # select white for all lines
  glColor3f(1.0, 1.0, 1.0)

  # in 1st row, 3 lines, each with a different stipple
  glEnable(GL_LINE_STIPPLE)
 

  glColor3f(1.0, 0.0, 0.0)

  drawLine(0,0,0, 10,0,0)

  # in 3rd row, 6 lines, with dash/dot/dash stipple  
  # as part of a single connected line strip        
  
  # in 4th row, 6 independent lines with same stipple  */
  # drawOneLine (50.0 + (i * 50.0), 50.0, 50.0 + ((i+1) * 50.0), 50.0)



  # in 5th row, 1 line, with dash/dot/dash stipple 
  # and a stipple repeat factor of 5               
  glLineStipple (5, 0x1C47)  #  dash/dot/dash 
  drawOneLine (50.0, 25.0, 350.0, 25.0)

  glDisable (GL_LINE_STIPPLE)
  glFlush ()

calpts=[[-1.16440105, 55.8322296, 3.51624513],
[-9.33136368, -36.3149834, 8.09555435],
[-17.9740868, -48.0383415, 5.88946438]]

campos = [0, 0, 5.0]

def drawStar(pt, size, incol, outcol):
  glBegin(GL_LINES)  
  glColor4f(incol[0], incol[1], incol[2], incol[3])
  glVertex3f(pt[0] - size, pt[1], pt[2])
  glColor4f(outcol[0], outcol[1], outcol[2], outcol[3])
  glVertex3f(pt[0] + size, pt[1], pt[2])
  glEnd()
  glBegin(GL_LINES)  
  glColor4f(incol[0], incol[1], incol[2], incol[3])
  glVertex3f(pt[0], pt[1] - size, pt[2])
  glColor4f(outcol[0], outcol[1], outcol[2], outcol[3])
  glVertex3f(pt[0], pt[1] + size, pt[2])
  glEnd()
  glBegin(GL_LINES)  
  glColor4f(incol[0], incol[1], incol[2], incol[3])
  glVertex3f(pt[0], pt[1], pt[2] - size)
  glColor4f(outcol[0], outcol[1], outcol[2], outcol[3])
  glVertex3f(pt[0], pt[1], pt[2] + size)
  glEnd()


def drawBasis(mat, opacity = 1.0):
   glColor4f(1.0, 0.0, 0.0, opacity)
   drawLine(0,0,0, mat[0],mat[1],mat[2])
   glColor4f(0.0, 1.0, 0.0, opacity)
   drawLine(0,0,0, mat[3],mat[4],mat[5])
   glColor4f(0.0, 0.0, 1.0, opacity)
   drawLine(0,0,0, mat[6],mat[7],mat[8])  

def init(): 
   glClearColor (0.0, 0.0, 0.0, 0.0)
   glShadeModel (GL_FLAT)

def display():

   glClear (GL_COLOR_BUFFER_BIT)
   glColor3f (1.0, 1.0, 1.0)
   glLoadIdentity ()             # clear the matrix 
   # viewing transformation 
   gluLookAt (campos[0],campos[1],campos[2], 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)
   glScalef (1.0, 1.0, 1.0)      # modeling transformation 
   glRotate(rotate[0], 1,0,0)
   glRotate(rotate[1], 0,1,0)
   glRotate(rotate[2], 0,0,1)
   glutWireCube (1.0)

   #drawBasis([1,0,0, 0,1,0, 0,0,1], 0.4)
   drawBasis(tracker_x + tracker_y + tracker_z)

   for i in calpts:
    drawStar(i, 2.4, [1, 0.8, 0, 0.1], [1, 1.0, 0, 1.0])
   
   glFlush ()

def reshape (w, h):
   glViewport (0, 0, w, h)
   glMatrixMode (GL_PROJECTION)
   glLoadIdentity ()
   glFrustum (-1.0, 1.0, -1.0, 1.0, 1, 2000.0)
   glMatrixMode (GL_MODELVIEW)

def idle():
    glutPostRedisplay()
    rotate[0] += spin[0]
    rotate[1] += spin[1]

MOUSE_ZOOM_SENSITIVITY = 0.1
MOUSE_SENSITIVITY = 0.5
SPIN_THRESHOLD = 2.0
SPIN_FACTOR = 0.0005
oldpos = [0, 0]
mousebuttonstatus = 0

def mousemove(x, y):
  oldpos[0] = x
  oldpos[1] = y

def mousemovedown(x, y):  
  diff = [oldpos[0] - x, oldpos[1] - y]
  print(mousebuttonstatus)
  if mousebuttonstatus == 2:

    campos[2] += diff[1] * MOUSE_ZOOM_SENSITIVITY
  else:
    len = vl.veclength(diff)
    if len > SPIN_THRESHOLD:
      spin[0] = spin[0] + (diff[1] ) * SPIN_FACTOR
      spin[1] = spin[1] + (diff[0] ) * SPIN_FACTOR
    else:
      spin[0] = spin[1] = 0.0  
    rotate[0] = rotate[0] + diff[1] * MOUSE_SENSITIVITY
    rotate[1] = rotate[1] + diff[0] * MOUSE_SENSITIVITY  
    oldpos[0] = x
    oldpos[1] = y  

def mouse(button, state, x, y): 
  global mousebuttonstatus
  mousebuttonstatus = button
 
    
    

def keyboard(key, x, y):
   
   if key == chr(27):
      import sys
      sys.exit(0)
   if key == 'A':
      spin[0] += 0.5

def visible(vis):
    if vis == GLUT_VISIBLE:
        glutIdleFunc(idle)
    else:
        glutIdleFunc(None)





#    glutSpecialFunc(special)
print("Fartkontrol")

#glUniform1f(0, 0.0)
print(sys.argv)
glutInit(sys.argv)

glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB)
glutInitWindowSize (500, 500)
glutInitWindowPosition (100, 100)

glutCreateWindow ('cube')
init ()
glutDisplayFunc(display)
glutReshapeFunc(reshape)

glutKeyboardFunc(keyboard)
glutMouseFunc(mouse)
glutVisibilityFunc(visible)

glutMotionFunc(mousemovedown)
glutPassiveMotionFunc(mousemove)

glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
glEnable(GL_BLEND)
print("Got here 6")
glutMainLoop()
