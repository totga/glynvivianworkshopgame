
#This is a comment. Everything after '#' is ignored by python
#We use comments to explain what bits of the code do
#You don't have to type these in your code if you don't want to: they won't affect
#how the program is run.
       
#Here, we use the function 'raw_input' that prints a message and waits
#for the user to type something.
#Once the user has typed something and hit 'return', the result is stored in #'name'.
#Note that in later version of python, we use 'input' instead of 'raw_input'
#'\n' means 'go to the next line'

name = raw_input("Hello, what's your name? \n")
#The colons (':') are important!
#They tells python that the instructions to do if the conditional is true #will be coming next

if name == "Tim":
    print("Oh, it's you " + name + ". I was expecting someone else.")
else:
    int("Hello " + name + ". I hope you're well!")
    for i in range(1, 12):
        if i != 12:
	        print(i)
    else:
        print(i)
