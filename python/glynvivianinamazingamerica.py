#!/usr/bin/python
import random
import sys
import pygame
print("Welcome to Glynn Vivian's Choose your own adventure!");


from util_adventure import *
InitGameGraphics("Glyn Vivian's Amazing America")

SetBackground(pygame.image.load('../img/glynvivadventurebg.png').convert_alpha())

WINDOWWIDTH = 1024	
WINDOWHEIGHT = 768

HALFW = WINDOWWIDTH / 2.0
HALFH = WINDOWHEIGHT / 2.0

FirstCompartment = AdventureRoom("a train carriage toilet.")
FirstCompartment.add_text("It appears to be fairly posh, for a train toilet")

FirstCompartment.image = pygame.image.load('../img/CorridorSections/Toilet.png').convert_alpha()

FirstCorridor = AdventureRoom("a section of carriage corridor")
FirstCorridor.image = pygame.image.load("../img/CorridorSections/Corridor1.png")
 
SecondCompartment = AdventureRoom("a train carriage bedroom.")
SecondCompartment.add_text("It appears to be nicely furnished but very small, even for a train bedroom")
SecondCorridor = AdventureRoom("a section of carriage corridor")
SecondCorridor.image = pygame.image.load("../img/CorridorSections/Corridor2.png")
SecondCompartment.image = pygame.image.load("../img/CorridorSections/Compartment2.png")

ThirdCompartment = AdventureRoom("a train carriage bedroom.")
ThirdCompartment.add_text("It appears to be nicely furnished but very small, even for a train bedroom")
ThirdCompartment.image = pygame.image.load("../img/CorridorSections/Compartment3.png")

ThirdCorridor = AdventureRoom("a section of carriage corridor")
ThirdCorridor.image = pygame.image.load("../img/CorridorSections/Corridor3.png")

ForthCompartment = AdventureRoom("a train carriage bedroom.")
ForthCompartment.add_text("It appears to be nicely furnished but very small, even for a train bedroom")
ForthCompartment.image = pygame.image.load("../img/CorridorSections/Compartment4.png")

ForthCorridor = AdventureRoom("a section of carriage corridor")
ForthCorridor.image = pygame.image.load("../img/CorridorSections/Corridor4.png")


FifthCompartment = AdventureRoom("a train carriage bedroom.")
FifthCompartment.add_text("It appears to be birght and colourful but very small, even for a train bedroom")
FifthCorridor = AdventureRoom("a section of carriage corridor")
FifthCorridor.image = pygame.image.load("../img/CorridorSections/Corridor5.png")


#FirstCorridor.add_text("")textRect

AdventureRoomExit(DIR_NORTH, FirstCorridor, FirstCompartment, "a sliding wooden door")
AdventureRoomExit(DIR_NORTH, SecondCorridor, SecondCompartment, "a sliding wooden door")
AdventureRoomExit(DIR_NORTH, ThirdCorridor, ThirdCompartment, "a sliding wooden door")
AdventureRoomExit(DIR_NORTH, ForthCorridor, ForthCompartment, "a sliding wooden door")
AdventureRoomExit(DIR_NORTH, FifthCorridor, FifthCompartment, "a sliding wooden door")
AdventureRoomExit(DIR_EAST, FirstCorridor, SecondCorridor, "you can travel down the corridor")
AdventureRoomExit(DIR_EAST, SecondCorridor, ThirdCorridor, "you can travel down the corridor")
AdventureRoomExit(DIR_EAST, ThirdCorridor, ForthCorridor, "you can travel down the corridor")
AdventureRoomExit(DIR_EAST, ForthCorridor,FifthCorridor , "you can travel down the corridor")

mirror=	AdventureObject("Mirror", "A Mirror; ","A Posh mirror with a beautiful frame: It's very beautiful", 100) 
rug = AdventureObject("Rug", "A Rug; fluffy and comfortable", "a fine rug made in persia", 20)
toliet = AdventureObject("Toliet", "A Toliet; ", "A very small white toliet; It's absoutelty minature!", 150)
bath= AdventureObject("Bath", "A Bath;Posh and expensive but very cramped; It looks very expensive", 300 )
soap= AdventureObject("Soap", "A bar of soap; it's smells sweatly of chamomile", 0.5 )
FirstCompartment.add_object(rug)
FirstCompartment.add_object(toliet)
FirstCompartment.add_object(mirror)
FirstCompartment.add_object(bath) 

AddTextLine("Welcome to Richard Glyn Vivian's Amazing America")

RunGameGraphics(SecondCompartment);
#RunGameConsole(FirstCompartment);        
        
