import SpriteSheet
from pygame import Rect
from pygame import sprite
import pygame
import math

class DepthSprite(pygame.sprite.Sprite):
    def __init__(self, image, pos, depth):
        pygame.sprite.Sprite.__init__(self)
        self.image =  image
        self.pos = pos
        self.worldpos = pos
        self.bounds = Rect(pos, (image.get_width(), image.get_height()))        
        self.rect = Rect(self.worldpos, (image.get_width(), image.get_height()))        
        self.rotation = 0        
        self.depth = depth
        self.damp  = 0.0
        
    def updatebounds(self):
        self.bounds = Rect(self.pos, self.rect.size)
        self.rect = Rect(self.worldpos, self.rect.size)        

    def set_pos(self, pos):
        self.pos = pos
        self.updatebounds()
       
    def get_pos(self):
        return self.pos

    def draw(surface):
        print("Drawing")
        surface.blit(image, self.worldpos, None)

    def reset(self):
        pass
    def update(self, camera):

        spos = self.get_pos()
        self.worldpos = ((spos[0] - camera.pos[0]) * self.depth + camera.offset[0],  (spos[1] - camera.pos[1]) * self.depth + camera.offset[1])            
        self.updatebounds()
        #print(self.worldpos)

    def move(self, diff):
        self.pos = (self.pos[0] + diff[0], self.pos[1] + diff[1])
        self.updatebounds()


    def unmove(self, diff):        
        self.pos = (self.pos[0] - diff[0], self.pos[1] - diff[1])
        self.updatebounds()



class PhysSprite(DepthSprite):
    
    def __init__(self, image, pos, depth):
        DepthSprite.__init__(self, image, pos, depth)
        self.velocity = (0,0)
        self.thrust = (0,0)
        
    
    def reset(self):
        #Velocity is damped each frame, thrust is zeroed.
        #reset thrust each frame. Thrust is instantaneous, adds to velocity. 
        self.thrust = (0,0)
        

    def jump(self, force):
        self.thrust = (self.thrust[0], self.thrust[1] + force)

    def apply_thrust(self, thrustval):
        self.thrust = (self.thrust[0] + thrustval[0], self.thrust[1] + thrustval[1])


    def update(self, camera):
        DepthSprite.update(self, camera)
        self.velocity = (self.velocity[0] + self.thrust[0], self.velocity[1] + self.thrust[1])
        self.move(self.velocity)
        self.velocity = (self.velocity[0] - self.velocity[0] * self.damp, self.velocity[1] - self.velocity[1] * self.damp)

class CollisionSprite(PhysSprite):
    def __init__(self, image, pos, depth):                
        super(CollisionSprite, self).__init__(image, pos, depth)
        self.collision_list = None        
    def update(self, camera):
        super(CollisionSprite, self).update(camera)
        if self.rect.bottom < 0:
            print("Colliding!")
            self.set_pos(self.rect.left, 0)

        if self.collision_list != None:
            for col in collision_list:
                if self.rect.colliderect(col.rect):
                    self.unmove(self.velocity)


class AnimSprite(PhysSprite):
    
    def __init__(self, images, pos, depth):                
        PhysSprite.__init__(self, images[0], pos, depth)                
        self.images = images
        print self.image
        self.imageidx = 0.0
        self.start_frame = 0
        self.end_frame = len(images)
        self.anim_speed = 0.1

    def set_anim_params(self, start , count, speed):
        self.start_frame = start
        self.end_frame = start + count
        self.speed = speed

    def update(self, camera):
        PhysSprite.update(self, camera)
        image_range = self.end_frame - self.start_frame
        intidx = int(math.floor(self.imageidx))
        self.image = self.images[self.start_frame + (intidx % image_range)]
        self.imageidx += self.anim_speed