#from numpy import *
#from scipy import *

import veclib as vl
import numpy as np
import scipy
from scipy import special, optimize
import matplotlib.pyplot as plt
import veclib

f = lambda x: -special.jv(3, x)
sol = optimize.minimize(f, 1.0)
x = np.linspace(0, 10, 5000)


#plt.plot(x, special.jv(3, x), '-', sol.x, -sol.fun, 'o')
#plt.show()


#def normalized(a, axis=-1, order=2):
#    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
#    l2[l2==0] = 1
#    return a / np.expand_dims(l2, axis)

x = vl.normalized([0.586705446, 0.795832276, 0.149758935])
y = vl.normalized([-0.343306750, 0.0769454464, 0.936066091])
z = vl.normalized([0.126320183, 0.991366148, -0.0351626314])


csxznumpy = vl.normalized(np.cross(z, x))
y2 = vl.normalized(vl.cross(x, z))

print ("x:  " + str(x))
print ("y:  " + str(y))
print ("z:  " + str(z))
#print ("Cross x-z (np) "+ str(csxznumpy))
#print ("Cross xz " + str(csxz))
print ("y2: " + str(y2))

for i in range(1, 128):	

	a=x[:]
	b=y[:]
	if i & 1 != 0:
		a[0] = -a[0]
	if i & 2 != 0:
		a[1] = -a[1]
	if i & 4 != 0:
		a[2] = -a[2]
	if i & 8 != 0:
		b[0] = -b[0]
	if i & 16 != 0:
		b[1] = -b[1]
	if i & 32 != 0:
		b[2] = -b[2]

	if i & 64 != 0:
		print ("z2: " + str(vl.normalized(vl.cross(a,b))))
	else:
		print ("z2: " + str(vl.normalized(vl.cross(b,a))))