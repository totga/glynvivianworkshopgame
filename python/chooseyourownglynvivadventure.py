#!/usr/bin/python
import random
import sys
print("Welcome to Glynn Vivian's Choose your own adventure!");


from util_adventure import *

import pygame, sys, random
from pygame.locals import *
import SpriteSheet
import SpriteStripAnim
import TSprite
import math
#from Sprite import Sprite
import veclib
import types
import random

WINDOWWIDTH = 1024	
WINDOWHEIGHT = 768

HALFW = WINDOWWIDTH / 2.0
HALFH = WINDOWHEIGHT / 2.0

 
FirstRoom = AdventureRoom("a brightly lit room.\n")
FirstRoom.add_text("It appears to be your bedroom")

Stairwell = AdventureRoom("a stairwell")
Foyer = AdventureRoom("the household foyer")
DrawingRoom = AdventureRoom("the drawing room.")

MusicRoom = AdventureRoom("the finely appointed music room")
WineCellar = AdventureRoom("the wine cellar")
WineCellar.add_text("It smells dank with a faint odour of past excess")

Kitchen = AdventureRoom("the kitchen. ")
Kitchen.add_text("pots and pans hang on the stained walls")
DiningRoom = AdventureRoom("the dining room.")
DiningRoom.add_text("you see extravagant crimson wallpaper")
GamesRoom = AdventureRoom("the games room")
GamesRoom.add_text("you have flash backs of spending time here as a child")
BallRoom = AdventureRoom("the ball room")
Study = AdventureRoom("the study")
DownstairsToilet = AdventureRoom("a toilet")
DownstairsToilet.add_text("the toilet smells of urine.")
ServantsQuatersCorridor = AdventureRoom("the servants quater's corridor")
ServantsQuatersCorridor2 = AdventureRoom("the servants quater's corridor continues")
ServantsQuatersCorridor3 = AdventureRoom("the servants quater's corridor continues")
ServantsQuatersCorridor4 = AdventureRoom("the servants quater's corridor continues")
Scullery = AdventureRoom("the scullery")
WestWingCorridor = AdventureRoom("the west wing corridor")
EastWingCorridor = AdventureRoom("the east wing corridor")
EastCourtyard = AdventureRoom("the east courtyard")
WestCourtyard = AdventureRoom("the west courtyard")
Conservatory = AdventureRoom("the conservatory")
Conservatory.add_text("the glass scatters light onto the floor")
KitchenGarden = AdventureRoom("the kitchen garden")
KitchenGarden.add_text("you can smell the herbs they tickle your nose")


servantsquaterstring = "a servant's quaters"
granddoor = "a grand wooden door"
corridor = "the corridor continues"

ServantsQuaters1 = AdventureRoom(servantsquaterstring)
ServantsQuaters2 = AdventureRoom(servantsquaterstring)
ServantsQuaters3 = AdventureRoom(servantsquaterstring)
ServantsQuaters4 = AdventureRoom(servantsquaterstring)
ServantsQuaters5 = AdventureRoom(servantsquaterstring)
ServantsQuaters6 = AdventureRoom(servantsquaterstring)
ServantsQuaters7 = AdventureRoom(servantsquaterstring)

AdventureRoomExit(DIR_EAST, FirstRoom, Stairwell, "a white washed door")
AdventureRoomExit(DIR_DOWN, Stairwell, Foyer, "Old wooden stairs")
AdventureRoomExit(DIR_EAST, Foyer, DrawingRoom, granddoor)
AdventureRoomExit(DIR_SOUTH, DrawingRoom, EastWingCorridor, granddoor)
AdventureRoomExit(DIR_WEST, Foyer, MusicRoom, granddoor)
AdventureRoomExit(DIR_SOUTH, EastWingCorridor, GamesRoom, granddoor)
AdventureRoomExit(DIR_WEST, EastWingCorridor, WestCourtyard, granddoor)
AdventureRoomExit(DIR_EAST, EastWingCorridor,DownstairsToilet, granddoor)
AdventureRoomExit(DIR_NORTH, Study, Conservatory, granddoor)
AdventureRoomExit(DIR_EAST, Conservatory, MusicRoom, granddoor)
AdventureRoomExit(DIR_SOUTH, MusicRoom, WestWingCorridor, granddoor)
AdventureRoomExit(DIR_EAST, WestWingCorridor, WestCourtyard, granddoor)

AdventureRoomExit(DIR_WEST, GamesRoom, DiningRoom, granddoor)
AdventureRoomExit(DIR_WEST, DiningRoom, BallRoom, granddoor)
AdventureRoomExit(DIR_SOUTH, DiningRoom, ServantsQuatersCorridor, granddoor)
AdventureRoomExit(DIR_NORTH, DiningRoom, WestWingCorridor, granddoor)
AdventureRoomExit(DIR_SOUTH, DiningRoom, ServantsQuatersCorridor, granddoor)
AdventureRoomExit(DIR_EAST, ServantsQuatersCorridor, Kitchen, granddoor)
AdventureRoomExit(DIR_EAST, Kitchen, Scullery, granddoor)
AdventureRoomExit(DIR_WEST, ServantsQuaters1, Kitchen, granddoor)
AdventureRoomExit(DIR_SOUTH, ServantsQuatersCorridor,ServantsQuatersCorridor2, corridor)
AdventureRoomExit(DIR_SOUTH, ServantsQuatersCorridor2,ServantsQuatersCorridor3, corridor)
AdventureRoomExit(DIR_SOUTH, ServantsQuatersCorridor3,ServantsQuatersCorridor4, corridor)

AdventureRoomExit(DIR_EAST, ServantsQuatersCorridor2,ServantsQuaters2, corridor)
AdventureRoomExit(DIR_WEST, ServantsQuatersCorridor2,ServantsQuaters3, corridor)
AdventureRoomExit(DIR_EAST, ServantsQuatersCorridor3,ServantsQuaters4, corridor)
AdventureRoomExit(DIR_WEST, ServantsQuatersCorridor3,ServantsQuaters5, corridor)
AdventureRoomExit(DIR_EAST, ServantsQuatersCorridor4,ServantsQuaters6, corridor)
AdventureRoomExit(DIR_WEST, ServantsQuatersCorridor4,ServantsQuaters7, corridor)

telephone = AdventureObject("Telephone", "A telephone", "This is a wealthy household: one of the first to have a telephone. Not too many people to phone at the moment.", 9)
telephonenotebook = AdventureObject("Notebook", "A Notebook", "Scribbled in black ink, the page is covered in dark marks", 9)
telephonetable = AdventureObject("Table", "A table", "A finely made Louis XV table: A stunning item, the owner has equisite taste.", 50)
telephonetable.add_object_on_top(telephone)
telephonetable.add_object_on_top(telephonenotebook)
Foyer.add_object(telephonetable)

bed = AdventureObject("Bed", "An iron framed bed", "Fine victorian bed", 100)
bear = AdventureObject("Bear", "A teddy bear", "A well loved teddy bear. Missing one eye.", 1)
FirstRoom.add_object(bed)
bed.add_object_on_top(bear)

set_current_room(FirstRoom)
print('You wake up in ')
get_current_room().printdescription()
while exit == False:
    
    input = raw_input("What do you want to do?")
    if get_current_room().respond(input) == False:
        introductions = ["You're in ", "You arrive in", "You are standing in", "You step in to ", "You are in ", "You have arrived in ", "You find yourself in "]
        print(introductions[random.randint(0, len(introductions) - 1)])
        get_current_room().printdescription()
        
        
