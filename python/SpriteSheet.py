#adapted from code found here:
#http://www.pygame.org/wiki/Spritesheet?parent=CookBook
#added support for alpha'd sprites

import pygame

class SpriteSheet(object):
    def __init__(self, filename):
        try:
            self.sheet = pygame.image.load(filename)
        except pygame.error, message:
            print 'Unable to load spritesheet image:', filename
            raise SystemExit, message
    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey = None):
        "Loads image from x,y,x+offset,y+offset"        
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size)        
        
        if colorkey is not None:
            
            if colorkey is -1:
                colorkey = image.get_at((0,0))                
            image.set_colorkey(colorkey, pygame.RLEACCEL)
            image.blit(self.sheet, (0, 0), rect)
        else:
            self.sheet.lock()
            pixels = pygame.PixelArray(self.sheet)
            outpixels = pygame.PixelArray(image)            
            offx = rect.left
            offy = rect.top
            height = image.get_height()
            width = image.get_width()
            for y in range(0, height):                
                for x in range(0, width):
                    pixel = pixels[x + offx, y+offy]
                    a = (pixel & 0xFF000000) >> 24 
                    r = (pixel & 0x00FF0000) >> 16 
                    g = (pixel & 0x0000FF00) >> 8 
                    b = (pixel & 0x000000FF)  
                    outpixels[x,y] = r | g << 8 | b << 16 | a << 24
            self.sheet.unlock()
            image.set_alpha(0)
        return image.convert_alpha()
    # Load a whole bunch of images and return them as a list
    def images_at(self, rects, colorkey = None):
        "Loads multiple images, supply a list of coordinates" 
        return [self.image_at(rect, colorkey) for rect in rects]
    # Load a whole strip of images
    def load_strip(self, rect, image_count, colorkey = None):
        "Loads a strip of images and returns them as a list"
        tups = [(rect[0]+rect[2]*x, rect[1], rect[2], rect[3])
                for x in range(image_count)]
        return self.images_at(tups, colorkey)
