import os

print(os.__file__)
# __init__.py

# Check compatibility
try:
  eval("1 if True else 2")
except SyntaxError:
  raise ImportError("requires ternary support")


path = os.path.dirname(os.__file__)
print(path)