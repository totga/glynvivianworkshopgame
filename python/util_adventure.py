import random
import pygame, sys, random
from pygame.locals import *
import SpriteSheet
import SpriteStripAnim
import TSprite
import math
#from Sprite import Sprite
import veclib
import types
from types import *


WINDOWWIDTH = 1024  
WINDOWHEIGHT = 768

HALFW = WINDOWWIDTH / 2.0
HALFH = WINDOWHEIGHT / 2.0
FONT_SIZE = 16

WHITE = (255,255,255)
TEXTCOLOR = WHITE
background = None


def InitGameConsole():
	print("Starting Artventure")    


def SetBackground(img):
	global background
	background = img

def InitGameGraphics(caption):
	print("Starting Artventure") 
	global FPSCLOCK, DISPLAYSURF, font, RESET_SURF, RESET_RECT, NEW_SURF, NEW_RECT, SOLVE_SURF, SOLVE_RECT    
	global movecode
	global player_object
	global spritelist
	global camera
	global background
	
	#camera = Camera((0,0))
	#camera.offset = (camera.offset[0], camera.offset[1] + 100)

	pygame.init()
	FPSCLOCK = pygame.time.Clock()
	DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT)) #pygame.FULLSCREEN
	pygame.display.set_caption(caption)
	font = pygame.font.Font('freesansbold.ttf', FONT_SIZE)
	background = None


class TextLines:
	def __init__(self):
		currentLine = []
		lines = []

	def addLine(self):
		self.lines.append(currentLine)
		self.currentLine = []

	def append(self, textBlock):
		if type(textBlock) is TextBlock:
			self.currentLine.append(textBlock)
		else:
			raise TypeError("Can only add TextBlocks to TextLines object")

	def append(self, text, colour, fontsize, image):
		self.currentLine.append(TextBlock(text, colour,fontsize,image))

	def popCurrentLine(self):
		ret = self.currentLine
		currentLine = []
		return ret

	def merge(self, other):
		for i in other.lines:
			self.lines.append(i)
		if len(other.currentLine > 0):
			self.lines.append(other.currentLine)




class TextBlock:

	def __init__(self, text, colour = (255,255,255,255),fontsize = 16, image = None):		
		self.text = text
		self.colour = colour 
		self.fontsize = fontsize
		self.image = image


class PlayerObject:
	def __init__(self):
		self.inventory = []
		self.health = 100
		self.capacity = 50


	def getinventory(self):
		descriptions = []
		for i in self.inventory:
			descriptions += i.getdescription()

		
player = PlayerObject() 
exit = False

directions_strings = ["north", "east", "south", "west", "north-east", "north-west", "south-east", "south-west", "up", "down"]

DIR_NORTH = 0
DIR_EAST = 1
DIR_SOUTH = 2
DIR_WEST = 3
DIR_NORTH_EAST = 4
DIR_NORTH_WEST = 5
DIR_SOUTH_EAST = 6
DIR_SOUTH_WEST = 7
DIR_UP = 8
DIR_DOWN  = 9

reverse_direction = [DIR_SOUTH, DIR_WEST, DIR_NORTH, DIR_EAST, DIR_SOUTH_WEST, DIR_SOUTH_EAST, DIR_NORTH_WEST, DIR_NORTH_EAST, DIR_DOWN, DIR_UP];

ObjectColour = (10,45,10,255)
ExitColour = (25,1,1,255)
UserColour = (1,1,25,255)

lineHeight = 20
indent = 5
linemovespeedscale = 0.1


textHistory = [] 

def AddTextLine(string, colour = (255,255,255,255), size = 12, image = None):
	global textHistory
	textHistory.append(TextBlock(string, colour, size, image))


class AdventureObject:
	def __init__(self, name, description, details, weight = 1):
		self.name = name
		self.synonyms = []
		self.description = description
		self.details = details
		self.weight = weight
		self.accepted_verbs = []
		self.objects_inside = []
		self.objects_on_top = []
		self.parent_list = None
		self.parent_object = None
		self.is_alive = False
		self.is_open = False
		self.image = None
   
	def perform_action(self, player, verb):
		if verb == "burn":
			return "you cannot do that"
		if verb == "open":
			is_open = True
		

	def answers_to_name(self, name):            
		return name == self.name.lower() or name in self.synonyms

	def add_synonym(self, synonym):
		synonyms.append(synonym)

	def getdescription(self):
		return [TextBlock(self.description, ObjectColour)]	   

	def use(self, target, context):
		return "You can't do that with this"
				
	def getdetails(self):
		details = []
		details.append(TextBlock(self.details))
		
		if (len(self.objects_on_top) > 0):
			details.append(TextBlock("There are some objects on top of it:"))
			for i in self.objects_on_top:
				details+=TextBlock(i.getdescription(), ObjectColour)
		if (len(self.objects_inside) > 0):
			details.append(TextBlock("There appears to be something inside"), ObjectColour)
	
	def add_object_inside(self, obj):
		self.objects_inside.append(obj)
		obj.parent_list = self.objects_inside
		obj.parent_object = self
		
	def add_object_on_top(self, obj):
		self.objects_on_top.append(obj)
		obj.parent_list = self.objects_on_top
		obj.parent_object = self

#def AdventureNPC(AdventureObject)

class AdventureRoomExit:
	def __init__(self, direction, room1, room2, description, is_locked = False, locked_message = ""):
		self.direction = direction
		self.description = description
		self.room1 = room1
		self.room2 = room2
		self.is_locked = is_locked
		self.locked_message = locked_message
		room1.exits[direction] = self
		room2.exits[reverse_direction[direction]] = self
		self.image = None

		#room.add_exit(AdventureRoomExit(oppose_direction, 
		
	def unlock(self):
		self.is_locked = false
		
	def getdescription(self, reverse):
		descriptions = []
		direction = reverse_direction[self.direction] if reverse else self.direction
		direction_string = ""

		if (direction >= DIR_UP):
			direction_string = directions_strings[direction].title() + ", there is an exit. It is "
		else:
			direction_string = "To the " + directions_strings[direction] + ", there is an exit. It is "
		if (len(self.description) > 0):
			direction_string += self.description
		descriptions.append(TextBlock(direction_string, ExitColour))
		return descriptions
		
g_current_room = None

def set_current_room(room):
	global g_current_room
	g_current_room = room

def get_current_room():
	global g_current_room
	return g_current_room


class AdventureRoom:   
	def __init__(self, text):
		self.text = text
		self.exits = {}      
		self.objects = []
		self.characters = []
		self.image = None

	def add_object(self, obj):
		self.objects.append(obj)
		obj.parent_list = self.objects
		obj.parent_object = self
		
	
		
	def add_text(self, txt):
		self.text = self.text + txt
		
	def getdescription(self):
		global directions_strings
		descriptions = []
		
		descriptions.append(TextBlock(self.text))
		objcount = len(self.objects)
		if objcount > 0:
			if objcount == 1:
				descriptions.append(TextBlock("There is one object in here: ")),
			else:
				descriptions.append(TextBlock("There are several items in the room: ")),
			for i in self.objects:
				descriptions += i.getdescription()

		exitcount = len(self.exits)
			
		if (exitcount > 0):
			if (exitcount > 1):
				descriptions.append(TextBlock("There are " + str(exitcount) + " exits ")),           
			#elif (exitcount == 1):
			#	descriptions.append("There is one exit: "),

			for i in self.exits:
				exit = self.exits[i]
				descriptions += exit.getdescription(exit.room1 != self)
		return descriptions
				
			
	def look_objects(self, objects, noun):
		details = []
		for i in objects:
			if i.name.lower() == noun:
				details += i.getdetails()
				return True, details
			elif len(i.objects_on_top) > 0:
				if self.look_objects(i.objects_on_top, noun):
					return True, details
		return False, None

	def find_object(self, objects, noun):
		for i in objects:
			if i.answers_to_name(noun):
				return i
			elif len(i.objects_on_top) > 0:
				#print ("checking list on top of " + i.name)
				j = self.find_object(i.objects_on_top, noun)
				if j != None:
					return j
		return None
		
		
	
	def respond(self, text):
		global g_current_room
		words = text.split(" ")
		if len(words) == 0:
			return
		descriptions = []
		verb = words[0].lower()
		
		noun = ""
		if len(words) > 1:
			noun = words[1].lower()
		
		openverbs = ["open"];
		moveverbs = ["go", "move", "exit", "travel"]
		takeverbs = ["take", "grab", "hold", "pick"]
		lookverbs = ["examine", "look", "inspect", "regard", "investigate"]
		attackverbs = ["attack", "hit", "punch", "throttle", "karate", "slap" , "kick"]
		annoyverbs = ["poke", "prod", "tickle", "bore", "annoy"]
		throwverbs = ["throw", "chuck", "hurl"]
		placeverbs = ["put", "place", "deposit"]
		inventoryverbs = ["inventory", "carrying", "bag", "briefcase"]
		useverts = ["use"]

		if verb in moveverbs:
			if noun == "game":
				exit = True
			
			if noun in directions_strings: 
				dir = directions_strings.index(noun)                
				if dir in self.exits:
					exit = self.exits[dir]
					if exit != None:                    
						if exit.is_locked == True:
							if exit.locked_message != "":
								descriptions.append(TextBlock(exit.locked_message))
							else:
								descriptions.append(TextBlock("You can't go in that direction: it's locked."))
						descriptions.append(TextBlock("You go " + directions_strings[exit.direction]))
						if g_current_room == exit.room1:
							g_current_room = exit.room2
						else:
							g_current_room = exit.room1                    
						return False, descriptions
				else:
					descriptions.append(TextBlock("You can't go in that direction."))
			else:
				descriptions.append(TextBlock("I don't recognise that direction."))
		elif verb in takeverbs:
			i = self.find_object(self.objects, noun)
			if i != None:                
				if i.weight < player.capacity:
					descriptions.append(TextBlock("You take the " + noun))
					player.inventory.append(i)
					i.parent_list.remove(i)
					player.capacity -= i.weight
				else:
					if i.weight < 10:
						descriptions.append(TextBlock("You can't carry any more - try dropping some items"))
					else:
						descriptions.append(TextBlock("You can't carry that, it's too heavy!"))
				return True, descriptions
			descriptions.append(TextBlock("That isn't here."))
			return True, descriptions
		elif verb in inventoryverbs:
			descriptions.append(player.getinventory())
		elif verb in annoyverbs:
			admonishments = ["Don't be annoying", "You can't annoy inanimate objects", "Silly person", "That's really annoying!"]
			descriptions.append(TextBlock(admonishments[random.randint(0, len(admonishments) - 1)]))
		elif verb in attackverbs:
			admonishments = ["This isn't a violent game!", "No hitting here", "Oi, don't be so brutal"]
			descriptions.append(TextBlock(admonishments[random.randint(0, len(admonishments) - 1)]))

		elif verb in lookverbs:
			descriptions.append(TextBlock("Looking"))
			if noun == "room" or noun == None or Noun == "":
				descriptions += getdescription(self)
			if self.look_objects(self.objects, noun) == False:
				descriptions.append(TextBlock("That isn't here."))
			return True, descriptions

		else:
			respones = ["I'm sorry, I didn't understand that.", "Can you rephrase that, please?", "Non comprende", "What?" ,"I did not understand what you just said"]
			descriptions.append(TextBlock((respones[random.randint(0, len(respones) - 1)])))
			
			return True, descriptions
		return False, descriptions


def terminate():
	pygame.quit()
	sys.exit()
	exit = True

introductions = ["You're in ", "You arrive in", "You are standing in", "You step in to ", "You are in ", "You have arrived in ", "You find yourself in "]

		

def RunGameConsole(FirstRoom):
	set_current_room(FirstRoom)
	print('You wake up in ')
	print(get_current_room().getdescription())
	while exit == False:
		input = raw_input("What do you want to do?")
		if get_current_room().respond(input) == False:
			print(introductions[random.randint(0, len(introductions) - 1)])
			descriptions = get_current_room().getdescription()
			for i in descriptions:
				print(i)            

laststartline = 0
textSurfaces = []

def SafeGet(array, i):
	if i >= 0 and i < len(array):
		return array[i]
	else:
		return None

def RunGameGraphics(FirstRoom):
	global textHistory 
	global laststartline
	global textSurfaces
	set_current_room(FirstRoom)	

	bordercolour = (12, 10, 10)
	pygame.key.set_repeat(1000, 500)
	current_command_string = ""

	textAreaRect = Rect(20, 600,  985, 140)
	displayAreaRect = Rect(99,68, 822, 492)

	textHistory.append(TextBlock(introductions[random.randint(0, len(introductions) - 1)]))
	textHistory += get_current_room().getdescription()
	  
	command_entered = False
	offset = 0
	offset_target = 0
	while (exit == False):
		DISPLAYSURF.set_clip(None)
		DISPLAYSURF.blit(background, (0,0), (0,0,WINDOWWIDTH, WINDOWHEIGHT))
		roomimage =  get_current_room().image
		#DISPLAYSURF.fill((100, 10, 10, 20), displayAreaRect)
		#DISPLAYSURF.fill((10, 10, 100, 20),textAreaRect)
		DISPLAYSURF.set_clip(displayAreaRect)

		if roomimage != None:
			DISPLAYSURF.blit(roomimage, (100,70), (0,0,roomimage.get_width(), roomimage.get_height()))
		for event in pygame.event.get(): # event handling loop
			if event.type == KEYDOWN:
				if event.key == K_UP:
					offset_target = offset + lineHeight					
				elif event.key == K_DOWN:
					offset_target = offset - lineHeight
				elif event.key == K_BACKSPACE:
					current_command_string = current_command_string[:-1]
				elif event.key == K_ESCAPE:
					terminate() # terminate if the KEYUP event was for the Esc key
				elif event.key == K_RETURN:
					command_entered = True
				else:
					offset_target = textAreaRect.height - (len(textHistory) + 1) * lineHeight
					current_command_string = current_command_string + event.unicode
			elif event.type == KEYUP:
				a = ""
			elif event.type == QUIT:
				terminate()

		if command_entered:
			textHistory.append(TextBlock(current_command_string, UserColour))
			offset_target = offset - lineHeight
			result, descriptions = get_current_room().respond(current_command_string)
			if result == False:
				textHistory.append(TextBlock(introductions[random.randint(0, len(introductions) - 1)]))
				roomdescription = get_current_room().getdescription()
				textHistory += roomdescription
				offset_target -= len(roomdescription) * lineHeight				
			else:
				textHistory += descriptions
				offset_target -= len(descriptions) * lineHeight

			

			#textHistory += get_current_room().getdescription()
	  
			current_command_string = ""
			command_entered = False            
		#pygame.event.post(event) # put the other KEYUP event objects back        
			
		offset += (offset_target - offset) * linemovespeedscale
		
	
#textRect
		DISPLAYSURF.set_clip(textAreaRect)
		
		
		startline = int(-offset / lineHeight)



		currentLine = startline		
		
		currYPos = textAreaRect.top + offset + currentLine * lineHeight
		linecount = textAreaRect.height / lineHeight
		left = textAreaRect.left + indent

		#only rerender text when needed
		if laststartline != startline or len(textSurfaces) == 0:			
			for i in range(0, linecount):
				lcurrentLine = i + currentLine
				s = SafeGet(textHistory, lcurrentLine)
				while(i >= len(textSurfaces)):	#ensure capacity
					textSurfaces.append(None)
					print("Ensuring capacity ")
				if s != None:
					
					textSurfaces[i] = font.render(s.text, 1, s.colour)
					print("Adding " + s.text + "Width: " + str(textSurfaces[i].get_width()))
				else:
					print("No line for" + str(lcurrentLine))
				
			print("startline: " + str(currentLine))		

		#clip 
		#if currentLine < 0:
		#	currYPos += currentLine * lineHeight
		#	currentLine = 0

		
		#pygame creates a new surface every time you render text, which isn't very efficient
		#for i in range(0, len(textHistory )):
		surfidx = 0
		for i in range(currentLine, currentLine + linecount + 1):			
			s = SafeGet(textHistory, i)
			if type(s) is types.StringType:
				print("Ooops, that wasn't a textblock: " + s)
			else:									
				textSurf = SafeGet(textSurfaces, surfidx)
				if textSurf != None:
					textRect = Rect(left, currYPos, textSurf.get_width(), textSurf.get_height())				
					DISPLAYSURF.blit(textSurf, textRect)

			currYPos += lineHeight
			surfidx = surfidx + 1
		
		textSurf = font.render(current_command_string, 1, UserColour)
		currYPos = textAreaRect.top + offset+ len(textHistory) * lineHeight
		textRect = Rect(left, currYPos , textSurf.get_width(), textSurf.get_height())
		DISPLAYSURF.blit(textSurf, textRect)

		laststartline = startline
		DISPLAYSURF.set_clip(None)

		pygame.display.update()
