import math
def cross(a, b):
	return [a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2],a[0]*b[1] - a[1]*b[0]]

def veclength(vec):	
	l = 0.0 
	for i in vec:		
		l = l + (i * i)			
	return math.sqrt(l)

def normalized(vec):
	l = 1.0 / veclength(vec)	
	return [vec[0] * l, vec[1] * l, vec[2] * l]
	
def normalized2(vec):
	l = 1.0 / veclength(vec)	
	return [vec[0] * l, vec[1] * l]

def vecneg(vec):
	return [-vec[0] ,-vec[1], -vec[2]]	

